### Answer to questions "How to improve the workshop?" ###
import numpy as np

def HowToImprove(checks):
    print("How would you improve this workshop?")
    print("Provide better information before the workshop:",int(checks[0]),"%")
    print("Clarify the workshop objectives:",int(checks[1]),"%")
    print("Reduce the content covered in the workshop:",int(checks[2]),"%")
    print("Increase the content covered in the workshop:",int(checks[11]),"%")
    print("Improve the instructional methods:",int(checks[3]),"%")
    print("Make workshop activities more stimulating:",int(checks[4]),"%")
    print("Improve workshop organization:",int(checks[5]),"%")
    print("Make the workshop less difficult:",int(checks[6]),"%")
    print("Make the workshop more difficult:",int(checks[12]),"%")
    print("Slow down the pace of the workshop:",int(checks[7]),"%")
    print("Speed up the pace of the workshop:",int(checks[13]),"%")
    print("Allot more time for the workshop:",int(checks[8]),"%")
    print("Shorten the time for the workshop:",int(checks[14]),"%")
    print("More tests/feedback during the workshop:",int(checks[9]),"%")
    print("Less participants to enable discussions in smaller groups:",int(checks[10]),"%")
