import numpy as np
from matplotlib import pyplot as plt

def eval_results(results1,results2,f_list1,f_list2,Npages):
    file1 = open("results_eval1.dat",'w')
    file2 = open("results_eval2.dat",'w')
    for i in range (Npages):
        if(i%4 == 0):
            for j in range(40):
                file2.write('{:6.6}'.format(str(results2[i,j]))+" ")
            file2.write("\n")
        if(i%4 == 2):
            for j in range(40):
                file1.write('{:6.6}'.format(str(results1[i,j]))+" ")
            file1.write("\n")    
    file1.close()
    file2.close()

    N_teacher = int(Npages/4)
    eval = np.zeros([N_teacher,85],dtype="int")
    ###correct answers###
    answer1 = [1,4,1,4,4,2,3,2,1,3,4,4,2,1,3,1,3,3,4,1,4,3,3,3,3,4,2,2,3,4,4,3,3,3,2,3,3,1,3,4]
    answer2 = [4,4,1,4,3,2,1,4,3,3,1,3,2,2,2,3,2,2,3,2,4,4,4,2,2,3,3,2,1,2,3,2,2,3,3,2,4,4,1,1]
    for i in range(N_teacher):
        i_2 = i*4
        i_1 = i*4+2
        
        for j in range(40):    
        #Eval1
            if(results1[i_1,j] == answer1[j]):
                eval[i,j] = 1
        #Eval2
            if(results2[i_2,j] == answer2[j]):
                eval[i,j+40] = 1
        eval[i,80] = np.sum(eval[i,0:80])
        eval[i,81] = np.sum(eval[i,0:30])
        eval[i,82] = np.sum(eval[i,40:70])
        eval[i,83] = np.sum(eval[i,30:40])
        eval[i,84] = np.sum(eval[i,70:80])
        if(eval[i,80] > 30):
            print(f_list2[i],eval[i,80],eval[i,82]-eval[i,81])
#        print(f_list1[i],f_list2[i],eval[i,:])

    print(eval[:,80])
    print('Total:','{:6.6}'.format(str(100*np.mean(eval[:,80])/(80))),'% are answered correctly')
#    print('Total:','{:6.6}'.format(str(100*np.mean(eval[:,80])/(80-QnA))),'% of answered questions are answered correctly')
    print('Eval1:','{:6.6}'.format(str(100*np.sum(eval[:,0:30])/(30*N_teacher))),'% of science questions are answered correctly')
    print('Eval1:','{:6.6}'.format(str(100*np.sum(eval[:,30:40])/(10*N_teacher))),'% of PSS questions are answered correctly')
    print('Eval2:','{:6.6}'.format(str(100*np.sum(eval[:,40:70])/(30*N_teacher))),'% of science questions are answered correctly')
    print('Eval2:','{:6.6}'.format(str(100*np.sum(eval[:,70:80])/(10*N_teacher))),'% of PSS questions are answered correctly')
    #Directly Compare questions with similar contents
    print('Question 1.1 vs. 2.3 (Wave):',int(100*np.mean(eval[:,0])),"% ",int(100*np.mean(eval[:,42])),"%")
    print('Question 1.2 vs. 2.4 (Wave):',int(100*np.mean(eval[:,1])),"% ",int(100*np.mean(eval[:,43])),"%")
    print('Question 1.4 vs. 2.7 (Planets):',int(100*np.mean(eval[:,3])),"% ",int(100*np.mean(eval[:,46])),"%")
    print('Question 1.10 vs. 2.20 (Trigonometry):',int(100*np.mean(eval[:,9])),"% ",int(100*np.mean(eval[:,59])),"%")
    print('Question 1.17 vs. 2.9 (Solve Lin. Equ.):',int(100*np.mean(eval[:,16])),"% ",int(100*np.mean(eval[:,48])),"%")
    print('Question 1.23 vs. 2.11 (Small Angle):',int(100*np.mean(eval[:,22])),"% ",int(100*np.mean(eval[:,50])),"%")
    print('Question 1.30 vs. 2.19 (Which Quad. Func.?):',int(100*np.mean(eval[:,29])),"% ",int(100*np.mean(eval[:,58])),"%")



    plt.hist(eval[:,80],bins=15)#,range=(1,1.02))
#    plt.show()
    plt.savefig("Results.pdf")
    plt.clf()

    plt.hist(eval[:,82]-eval[:,81],bins=15)#,range=(1,1.02))
#    plt.show()
    plt.savefig("Improve_diff.pdf")
    plt.clf()

    plt.hist(eval[:,81]*(100/30),bins=15,alpha=0.75,label="before")#,range=(1,1.02))
    plt.hist(eval[:,82]*(100/30),bins=15,alpha=0.75,label="after")#,range=(1,1.02))
#    plt.show()
    plt.legend()
    plt.xlabel("% correct")
    plt.xlim([0,100])
    plt.savefig("Improve_comp.pdf")
    plt.clf()
