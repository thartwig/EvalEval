#run with: "python main_eval.py"

### EvalEval ###
#This code should help to evaluate evaluations
#Especially, it can interpret the hand-written answers of multiple-choice questions
#For this, the answers need to be in scanned (pdf/png) format and the expected answers need to be roughly in the same region of the scanned page.
#The code style is horrible and not optimized but it works :-)
#Please use the code freely (for non-commercial purposes) and contact me if you have any questions:
#Tilman.Hartwig@ipmu.jp
#Please note, the code can not deal with "corrected answers" as it will always pick the darkest cell. At the moment, such answers must be treated (whitened) by hand before.

###  Version 1.0  ###
### November 2019 ###

###Quick Start Guide###
#If you have a pdf of scanned pages, FileName.pdf, then execute
#   convert FileName.pdf Example-%03d.png
#to convert pdf into pngs with ImageMagick
#All files and main_eval.py should be in the same folder.
#Run the code first with calibrate = True (see below) and check all places in the code with ###CALIBRATE### and set them to your specific problem accordingly.

#import libraries
import numpy as np
import time
import glob
from matplotlib import pyplot as plt
from PIL import Image
from results_eval import eval_results
from improve import HowToImprove

#first run code with this option in order to calibrate code to your specific questionnaires.
calibrate = True #output additional infos that help to calibrate
#look for   ###CALIBRATE###   in the code to adapt code to your data

show = True
#In this mode, the program should display your scanned pages with grey boxes, where the code expects the answers to be.

listing = sorted(glob.glob("Example-*.png"))
Npages = len(listing)
page = 0

#arrays to save results
results1 = np.zeros([Npages,40])
results2 = np.zeros([Npages,40])
checks = np.zeros(15)
scale5 = np.zeros([9,6])
table7 = np.zeros([6,4])
table11 = np.zeros([13,6])

N_notAnswered = 0

offset = 7#for finding limiting lines of tables
pat = 3#for finding multiple-choice marks

if calibrate:
#can calibrate either Eval1 or Eval2 at the same time
    all_x1 = []
    all_x2 = []
    all_y1 = []
    all_y2 = []
    all_blacks1 = []
    all_blacks2 = []
    all_blacks21 = []
    all_blacksc = []
    all_blacks7 = []
    all_blacks11 = []
    
    all_ratios12 = []

#In our original evaluation, each teachers answered various questions of four different sheets. So there are 60 x 4 sheets in our original data set.

#page order
#1 +4n: eval2 multiple-choice
#2 +4n: eval2 opinion
#3 +4n: eval1 multiple-choice
#4 +4n: eval1 opinion

f_list1 = []
f_list2 = []

for f in listing:
    #go through pages
    print(f)
    page += 1
    if(page%4 == 1):
        print("page:",page,"/",Npages)
        f_list1.append(f)
        print("Multiple Choice: Eval2")
        im = Image.open(f).convert('L') #convert to gray without alpha channel
        pix = im.load()
        width, height = im.size  # Get the width and hight of the image for iterating over
        black_max = 255*width*height

###CALIBRATE###
        #boundaries of tables (need to be set to pixels where you expect boundaries of table)
        x1 = 82
        x2 = 477
        y1 = 343
        y2 = 708

###CALIBRATE###
    #relative spacing within table
        dx1 = 0.0523
        dx2 = 0.0517
        dy = 0.04348

    #find dark lines as limitations of array (choose "blackest" line)
    ###x1###
        black_min = black_max
        i_min = -1
        for i in range(x1-offset,x1+offset):
            black = 0
            for j in range(y1-offset,y2+offset):
                black += pix[i,j]
            if(black < black_min):
                black_min = black
                i_min = i
        x1_min = i_min
        if calibrate:
            for j in range(y1-offset,y2+offset):
                pix[i_min,j] = 0
    ###x2###
        black_min = black_max
        i_min = -1
        for i in range(x2-offset,x2+offset):
            black = 0
            for j in range(y1-offset,y2+offset):
                black += pix[i,j]
            if(black < black_min):
                black_min = black
                i_min = i
        x2_min = i_min
        if calibrate:
            for j in range(y1-offset,y2+offset):
                pix[i_min,j] = 0
    ###y1###
        black_min = black_max
        i_min = -1
        for i in range(y1-offset,y1+offset):
            black = 0
            for j in range(x1-offset,x2+offset):
                black += pix[j,i]
            if(black < black_min):
                black_min = black
                i_min = i
        y1_min = i_min
        if calibrate:
            for j in range(x1-offset,x2+offset):
                pix[j,i_min] = 0
    ###y2###
        black_min = black_max
        i_min = -1
        for i in range(y2-offset,y2+offset):
            black = 0
            for j in range(x1-offset,x2+offset):
                black += pix[j,i]
            if(black < black_min):
                black_min = black
                i_min = i
        y2_min = i_min
        if calibrate:
            for j in range(x1-offset,x2+offset):
                pix[j,i_min] = 0

#This can help to calibrate, i.e. to find the mean of x1,x2,y1,y2
#        if calibrate:
#            print("x1,x2,y1,y2:",x1_min,x2_min,y1_min,y2_min)
#            all_x1.append(x1_min)
#            all_x2.append(x2_min)
#            all_y1.append(y1_min)
#            all_y2.append(y2_min)
        L_x = x2_min-x1_min
        L_y = y2_min-y1_min
        
        di_x = int(round(L_x*dx1))
        di_y = int(round(L_y*dy))
        for i in range(20):#go through first column
###CALIBRATE###
            y = int(round(y1_min+L_y*(0.1292+i*dy)))#set top corner
            blacks = np.zeros(4)
            #go through four cells and find darkest cell
            for j in range(4):
                black = 0
###CALIBRATE###
                x = int(round(x1_min+L_x*(0.3515+j*dx1)))#set left corner
                for k in range(x+pat,x+di_x-pat):
                    for l in range(y+pat,y+di_y-pat):
                        black += pix[k,l]
                        if calibrate:
                            pix[k,l] = 100
                blacks[j] = black
#blacks distribution can help to find threshold between intentional marks and "dirt"
#                if calibrate:
#                    all_blacks1.append(black)
            j_min = np.argmin(blacks)
            sorted = np.sort(blacks)
###CALIBRATE###
            cut = 37300#set to distinguish between (un)intentional answers
            ratio = sorted[1]/sorted[0]
            if(np.amin(blacks) > cut*0.98 and ratio < 1.01):
                N_notAnswered += 1
                if calibrate:
                    print(i+1,"PROBLEM1: NO answer marked?",ratio)
                    all_ratios12.append(ratio)
                j_min = -2
            elif(sorted[1] < cut):
                print(i+1,"PROBLEM2: TWO answers marked?",sorted)
                j_min = -2
            results2[page-1,i] = j_min+1#python index offset
            
    #SECOND COLUMN
        di_x = int(round(L_x*dx2))
        for i in range(20):
###CALIBRATE###
            y = int(round(y1_min+L_y*(0.1292+i*dy)))
            blacks = np.zeros(4)
            #go through four cells
            for j in range(4):
                black = 0
###CALIBRATE###
                x = int(round(x1_min+L_x*(0.7411+j*dx1)))
                for k in range(x+pat,x+di_x-pat):
                    for l in range(y+pat,y+di_y-pat):
                        black += pix[k,l]
                        if calibrate:
                            pix[k,l] = 100
                blacks[j] = black
#                if calibrate:
#                    all_blacks2.append(black)
            j_min = np.argmin(blacks)
            sorted = np.sort(blacks)
###CALIBRATE###
            cut = 34900
            ratio = sorted[1]/sorted[0]
            if(np.amin(blacks) > cut*0.98 and ratio < 1.01):
                N_notAnswered += 1
                if calibrate:
                    print(i+1+20,"PROBLEM3: NO answer marked?",ratio)
                    all_ratios12.append(ratio)
                j_min = -2
            elif(sorted[1] < cut):
                print(i+1+20,"PROBLEM4: TWO answers marked?",sorted)
                j_min = -2
            results2[page-1,i+20] = j_min+1#python index offset

####Check MARKS####
        for i in range(15):#go through checked questions
            ii = i
            if(i==11):
                ii = 2
            elif(i>11):
                ii = i-11+5
###CALIBRATE###
            y = int(round(y1_min+L_y*(-0.606+(ii-1)*0.04651)))#set top corner
            blacks = np.zeros(2)
            #go through two cells (compare grayscales)
            for j in range(2):
                black = 0
###CALIBRATE###
                x = int(round(x1_min+L_x*(0.0036-0.025+j*0.034)))#set left corner
                if(i>=11):
###CALIBRATE###
                    x = int(round(x1_min+L_x*(0.5796+j*0.034)))
###CALIBRATE###
                for k in range(x+pat-1,x+14-pat):#first -1 to capture check mark
                    for l in range(y+pat,y+18-pat-1):#last -1 in order not to take the line
                        black += pix[k,l]
                        if calibrate:
                            pix[k,l] = 100
                blacks[j] = black
                if calibrate:
                    all_blacksc.append(black)
###CALIBRATE###
            if(np.amin(blacks)<24700):
                checks[i] += 1
                if calibrate:
                    print("Found Check",i)
        if show:
            im.show()
            time.sleep(3)

###########
###########


    if(page%4 == 3):
        f_list2.append(f)
        print("Multiple Choice: Eval1")
        im = Image.open(f).convert('L') #convert to gray without alpha channel
        pix = im.load()
        width, height = im.size  # Get the width and hight of the image for iterating over
        black_max = 255*width*height

    #boundaries of tables
###CALIBRATE###
        x1 = 71
        x2 = 493
        y1 = 104
        y2 = 518

    #relative spacing within table
###CALIBRATE###
        dx1 = 0.0523
        dx2 = 0.0517
        dy = 0.04348

    #find dark lines as limitations of array
    ###x1###
        black_min = black_max
        i_min = -1
        for i in range(x1-offset,x1+offset):
            black = 0
            for j in range(y1-offset,y2+offset):
                black += pix[i,j]
            if(black < black_min):
                black_min = black
                i_min = i
        x1_min = i_min
        if calibrate:
            for j in range(y1-offset,y2+offset):
                pix[i_min,j] = 0
    ###x2###
        black_min = black_max
        i_min = -1
        for i in range(x2-offset,x2+offset):
            black = 0
            for j in range(y1-offset,y2+offset):
                black += pix[i,j]
            if(black < black_min):
                black_min = black
                i_min = i
        x2_min = i_min
        if calibrate:
            for j in range(y1-offset,y2+offset):
                pix[i_min,j] = 0
    ###y1###
        black_min = black_max
        i_min = -1
        for i in range(y1-offset,y1+offset):
            black = 0
            for j in range(x1-offset,x2+offset):
                black += pix[j,i]
            if(black < black_min):
                black_min = black
                i_min = i
        y1_min = i_min
        if calibrate:
            for j in range(x1-offset,x2+offset):
                pix[j,i_min] = 0
    ###y2###
        black_min = black_max
        i_min = -1
        for i in range(y2-offset,y2+offset):
            black = 0
            for j in range(x1-offset,x2+offset):
                black += pix[j,i]
            if(black < black_min):
                black_min = black
                i_min = i
        y2_min = i_min
        if calibrate:
            for j in range(x1-offset,x2+offset):
                pix[j,i_min] = 0

#        if calibrate:
#            print("x1,x2,y1,y2:",x1_min,x2_min,y1_min,y2_min)
#            all_x1.append(x1_min)
#            all_x2.append(x2_min)
#            all_y1.append(y1_min)
#            all_y2.append(y2_min)
        L_x = x2_min-x1_min
        L_y = y2_min-y1_min
        
        di_x = int(round(L_x*dx1))
        di_y = int(round(L_y*dy))
        for i in range(20):#go through first column
###CALIBRATE###
            y = int(round(y1_min+L_y*(0.1292+i*dy)))
            blacks = np.zeros(4)
            #go through four cells
            for j in range(4):
                black = 0
###CALIBRATE###
                x = int(round(x1_min+L_x*(0.3515+j*dx1)))
                for k in range(x+pat,x+di_x-pat):
                    for l in range(y+pat,y+di_y-pat):
                        black += pix[k,l]
                        if calibrate:
                            pix[k,l] = 100
                blacks[j] = black
                if calibrate:
                    all_blacks1.append(black)
            j_min = np.argmin(blacks)
            sorted = np.sort(blacks)
###CALIBRATE###
            cut = 48300
            ratio = sorted[1]/sorted[0]
            if(np.amin(blacks) > cut*0.98 and ratio < 1.01):
                N_notAnswered += 1
                if calibrate:
                    print(i+1,"PROBLEM5: NO answer marked?",ratio)
                    all_ratios12.append(ratio)
                j_min = -2
            elif(sorted[1] < cut):
                print(i+1,"PROBLEM6: TWO answers marked?",sorted)
                j_min = -2
            results1[page-1,i] = j_min+1#python index offset
            
    #SECOND COLUMN
        di_x = int(round(L_x*dx2))
        for i in range(20):
###CALIBRATE###
            y = int(round(y1_min+L_y*(0.1292+i*dy)))
            blacks = np.zeros(4)
            #go through four cells
            for j in range(4):
                black = 0
###CALIBRATE###
                x = int(round(x1_min+L_x*(0.7411+j*dx1)))
                for k in range(x+pat,x+di_x-pat):
                    for l in range(y+pat,y+di_y-pat):
                        black += pix[k,l]
                        if calibrate:
                            pix[k,l] = 100
                blacks[j] = black
                if calibrate:
                    all_blacks2.append(black)
            j_min = np.argmin(blacks)
            sorted = np.sort(blacks)
###CALIBRATE###
            cut = 48500
            ratio = sorted[1]/sorted[0]
            if(np.amin(blacks) > cut*0.98 and ratio < 1.01):
                N_notAnswered += 1
                if calibrate:
                    print(i+1+20,"PROBLEM7: NO answer marked?",ratio)
                    all_ratios12.append(ratio)
                j_min = -2
            elif(sorted[1] < cut):
                print(i+1+20,"PROBLEM8: TWO answers marked?",sorted)
                j_min = -2
            results1[page-1,i+20] = j_min+1#python index offset
        if show:
            im.show()
            time.sleep(3)


##### Page 1 of Eval 2 ####
    if(page%4 == 2):
        f_list2.append(f)
        print("Post Eval: Opinions")
        im = Image.open(f).convert('L') #convert to gray without alpha channel
        pix = im.load()
        width, height = im.size  # Get the width and hight of the image for iterating over
        black_max = 255*width*height

    #boundaries of tables
###CALIBRATE###
        x1 = 82
        x2 = 480
        y1 = 221
        y2 = 380

    #relative spacing within table
###CALIBRATE###
        dx = 0.04917
        dy = 0.1

    #find dark lines as limitations of array
    ###x1###
        black_min = black_max
        i_min = -1
        for i in range(x1-offset,x1+offset):
            black = 0
            for j in range(y1-offset,y2+offset):
                black += pix[i,j]
            if(black < black_min):
                black_min = black
                i_min = i
        x1_min = i_min
        if calibrate:
            for j in range(y1-offset,y2+offset):
                pix[i_min,j] = 0
    ###x2###
        black_min = black_max
        i_min = -1
        for i in range(x2-offset,x2+offset):
            black = 0
            for j in range(y1-offset,y2+offset):
                black += pix[i,j]
            if(black < black_min):
                black_min = black
                i_min = i
        x2_min = i_min
        if calibrate:
            for j in range(y1-offset,y2+offset):
                pix[i_min,j] = 0
    ###y1###
        black_min = black_max
        i_min = -1
        for i in range(y1-offset,y1+offset):
            black = 0
            for j in range(x1-offset,x2+offset):
                black += pix[j,i]
            if(black < black_min):
                black_min = black
                i_min = i
        y1_min = i_min
        if calibrate:
            for j in range(x1-offset,x2+offset):
                pix[j,i_min] = 0
    ###y2###
        black_min = black_max
        i_min = -1
        for i in range(y2-offset,y2+offset):
            black = 0
            for j in range(x1-offset,x2+offset):
                black += pix[j,i]
            if(black < black_min):
                black_min = black
                i_min = i
        y2_min = i_min
        if calibrate:
            for j in range(x1-offset,x2+offset):
                pix[j,i_min] = 0


#        if calibrate:
#            print("x1,x2,y1,y2:",x1_min,x2_min,y1_min,y2_min)
#            all_x1.append(x1_min)
#            all_x2.append(x2_min)
#            all_y1.append(y1_min)
#            all_y2.append(y2_min)
        
        L_x = x2_min-x1_min
        L_y = y2_min-y1_min
        
        di_x = int(round(L_x*dx))
        di_y = int(round(L_y*dy))
        for i in range(9):#go through lines
            y = int(round(y1_min+L_y*((i+1)*dy)))
            blacks = np.zeros(5)
            #go through four cells
            for j in range(5):
                black = 0
###CALIBRATE###
                x = int(round(x1_min+L_x*(0.7541+j*dx)))
                for k in range(x+pat,x+di_x-pat):
                    for l in range(y+pat,y+di_y-pat):
                        black += pix[k,l]
                        if calibrate:
                            pix[k,l] = 100
                blacks[j] = black
                if calibrate:
                    all_blacks21.append(black)
###CALIBRATE###
            if(np.amin(blacks) < 35200):
                j_min = int(np.argmin(blacks))
                scale5[i,j_min] += 1
                scale5[i,5] += 1
                
#####SECOND TABLE#####
###CALIBRATE###
        dx = 0.13
        dy = 0.09975
        di_x = int(round(L_x*dx))
        di_y = int(round(L_y*dy))
        for i in range(6):#go through lines
###CALIBRATE###
            y = int(round((439*L_y/169)+y1_min+L_y*((i+1)*dy))+2)
            blacks = np.zeros(3)
            #go through four cells
            for j in range(3):
                black = 0
###CALIBRATE###
                x = int(round(x1_min+L_x*(0.5343+j*dx)))
                for k in range(x+pat,x+di_x-pat):
                    for l in range(y+pat,y+di_y-pat):
                        black += pix[k,l]
                        if calibrate:
                            pix[k,l] = 100
                blacks[j] = black
                if calibrate:
                    all_blacks7.append(black)
###CALIBRATE###
            if(np.amin(blacks) < 116400):
                j_min = int(np.argmin(blacks))
                table7[i,j_min] += 1
                table7[i,3] += 1                

        if show:
            im.show()
            time.sleep(3)



##### Page 1 of Eval 1 ####
    if(page%4 == 0):
        f_list2.append(f)
        print("Pre Eval: Opinions")
        im = Image.open(f).convert('L') #convert to gray without alpha channel
        pix = im.load()
        width, height = im.size  # Get the width and hight of the image for iterating over
        black_max = 255*width*height

    #boundaries of tables
###CALIBRATE###
        x1 = 68
        x2 = 456
        y1 = 404
        y2 = 530

    #relative spacing within table
###CALIBRATE###
        dx = 0.056
        dy = 0.1429

    #find dark lines as limitations of array
    ###x1###
        black_min = black_max
        i_min = -1
        for i in range(x1-offset,x1+offset):
            black = 0
            for j in range(y1-offset,y2+offset):
                black += pix[i,j]
            if(black < black_min):
                black_min = black
                i_min = i
        x1_min = i_min
        if calibrate:
            for j in range(y1-offset,y2+offset):
                pix[i_min,j] = 0
    ###x2###
        black_min = black_max
        i_min = -1
        for i in range(x2-offset,x2+offset):
            black = 0
            for j in range(y1-offset,y2+offset):
                black += pix[i,j]
            if(black < black_min):
                black_min = black
                i_min = i
        x2_min = i_min
        if calibrate:
            for j in range(y1-offset,y2+offset):
                pix[i_min,j] = 0
    ###y1###
        black_min = black_max
        i_min = -1
        for i in range(y1-offset,y1+offset):
            black = 0
            for j in range(x1-offset,x2+offset):
                black += pix[j,i]
            if(black < black_min):
                black_min = black
                i_min = i
        y1_min = i_min
        if calibrate:
            for j in range(x1-offset,x2+offset):
                pix[j,i_min] = 0
    ###y2###
        black_min = black_max
        i_min = -1
        for i in range(y2-offset,y2+offset):
            black = 0
            for j in range(x1-offset,x2+offset):
                black += pix[j,i]
            if(black < black_min):
                black_min = black
                i_min = i
        y2_min = i_min
        if calibrate:
            for j in range(x1-offset,x2+offset):
                pix[j,i_min] = 0

        if calibrate:
            print("x1,x2,y1,y2:",x1_min,x2_min,y1_min,y2_min)
            all_x1.append(x1_min)
            all_x2.append(x2_min)
            all_y1.append(y1_min)
            all_y2.append(y2_min)
        
        L_x = x2_min-x1_min
        L_y = y2_min-y1_min
        
        di_x = int(round(L_x*dx))
        di_y = int(round(L_y*dy))
        for i in range(13):#go through lines
            y = int(round(y1_min+L_y*((i+1)*dy)))
            if (i>5):
###CALIBRATE###
                y = y + int(round(dy*L_y+39*L_y/125.5)) 
            blacks = np.zeros(5)
            #go through four cells
            for j in range(5):
                black = 0
###CALIBRATE###
                x = int(round(x1_min+L_x*(0.72+j*dx)))
                for k in range(x+pat,x+di_x-pat):
                    for l in range(y+pat,y+di_y-pat):
                        black += pix[k,l]
                        if calibrate:
                            pix[k,l] = 100
                blacks[j] = black
                if calibrate:
                    all_blacks11.append(black)
###CALIBRATE###
            if(np.amin(blacks) < 48000):
                j_min = int(np.argmin(blacks))
                table11[i,j_min] += 1
                table11[i,5] += 1
        if show:
            im.show()
            time.sleep(3)

checks = 100*checks/float(Npages/4)
print(checks)

if calibrate:
    print("Set threshold for raising problems above to the value that best separates the highest peak")
    plt.hist(all_blacks11,bins=32,range=(45000,50000),log=True)
    plt.show()
    plt.clf()

    plt.hist(all_blacks7,bins=32,range=(110000,120000))
    plt.show()
    plt.clf()

    plt.hist(all_blacks21,bins=32,log=True,range=(34000,38000))
    plt.show()
    plt.clf()

    plt.hist(all_blacksc,bins=32,range=(24000,25500),log=True)
    plt.show()
    plt.clf()

    plt.hist(all_blacks1,bins=32,range=(46000,49000),log=True)
    plt.show()
    plt.clf()

    plt.hist(all_blacks2,bins=32,range=(47000,49000),log=True)
    plt.show()
    plt.clf()
    
    plt.hist(all_ratios12,bins=32,log=True)#,range=(1,1.02))
    plt.show()
    plt.clf()

    
    print("Set Boundaries of table x1, x2, y1, y2 to these values:")
    print(np.median(all_x1))
    print(np.median(all_x2))
    print(np.median(all_y1))
    print(np.median(all_y2))

#Now, you can analyse your data, e.g.:

#print(results1[0:4,:])
#print(results2[0:4,:])
#print(scale5)
print(table11)

#QnA = N_notAnswered/(Npages/4)
#print("Number of not answered questions: ",N_notAnswered)
#print("Number of not answered questions per teacher: ",QnA)

eval_results(results1,results2,f_list1,f_list2,Npages)
HowToImprove(checks)

